require 'rails_helper'

RSpec.describe "challenges/edit", type: :view do
  before(:each) do
    @challenge = assign(:challenge, Challenge.create!(
      :name => "MyString",
      :description => "MyString is at least 20 characters",
      :moderation_flag => false,
      :status => "MyString",
      :terms => "MyText"
    ))
  end

  it "renders the edit challenge form" do
 	render

    assert_select "form[action=?][method=?]", challenge_path(@challenge), "post" do

      assert_select "input#challenge_name[name=?]", "challenge[name]"

      assert_select "textarea#challenge_description[name=?]", "challenge[description]"

      assert_select "input#challenge_moderation_flag[name=?]", "challenge[moderation_flag]"

      assert_select "input#challenge_status[name=?]", "challenge[status]"

      assert_select "textarea#challenge_terms[name=?]", "challenge[terms]"
    end
  end
end
