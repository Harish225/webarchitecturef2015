class Comment < ActiveRecord::Base

  belongs_to :user
  belongs_to :challenge
  
  has_many :vote_comments

  validates :comment,           presence: true,
                                length: { within: 20..250 }

end
