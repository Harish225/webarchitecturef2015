json.array!(@vote_challenges) do |vote_challenge|
  json.extract! vote_challenge, :id, :user_id, :challenge_id
  json.url vote_challenge_url(vote_challenge, format: :json)
end
